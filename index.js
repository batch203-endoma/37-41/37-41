const express = require("express");
const mongoose = require("mongoose");
//Allows our backend application to be available with our frontend application
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js")
const courseRoutes = require("./routes/courseRoutes.js")
const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.wvosk1v.mongodb.net/b203_bookingAPI?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

//This syntax will allow flexibility when using the application locally or as a hosted app.
const port = process.env.PORT || 4000;

//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
//Defines the "/users" string tobe included for all user routes in the "userRoutes" file.
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


//Listen
app.listen(port, () => {
  console.log(`API is now online on port ${port}`)
})
