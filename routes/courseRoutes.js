const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js")

console.log(courseControllers);

//Route for Creating course
router.post("/", auth.verify, courseControllers.addCourse);

//Route for viewing all courses
router.get("/all", auth.verify, courseControllers.getAllCourses);

//Route for viewing all active courses
router.get("/", courseControllers.getAllActive);

//route for viewing a specific course
router.get("/:courseId", courseControllers.getCourse);

//Route for updating a course
router.put("/:courseId", auth.verify, courseControllers.updateCourse)

//Route for archiving a course

router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);


module.exports = router;
