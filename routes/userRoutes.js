const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js")

console.log(userControllers);
router.post("/checkEmail", userControllers.checkEmailExist);

//Route for user registration
router.post("/register", userControllers.registerUser);
//route for user authentication
router.post("/login", userControllers.loginUser);

//route for details.
router.get("/details", auth.verify, userControllers.getProfile);

//Route for enrolling a user
router.post("/enroll", auth.verify, userControllers.enroll);


module.exports = router;
