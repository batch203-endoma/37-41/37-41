/*
user {

  id - unique identifier for the document,
  firstName,
  lastName,
  email,
  password,
  mobileNumber,
  isAdmin,
  enrollments: [
    {

      id - document identifier,
      courseId - the unique identifier for the course,
      courseName - optional,
      status,
      isPaid,
      dateEnrolled
    }
  ]

}


course {

  id - unique for the document (auto generated)
  name,
  description,
  price,
  slots,
  isActive,
  createdOn,
  enrollees: [

    {
      id - document identifier (auto generated),
      userId,
      email,
      isPaid,
      dateEnrolled
    }
  ]

}
*/

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required"]
  },
  lastName: {
    type: String,
    required: [true, "Last name is required"]
  },
  email: {
    type: String,
    required: [true, "email is required"]
  },
  password: {
    type: String,
    //select:false,
    required: [true, "Password is required"]
  },
  mobileNumber: {
    type: String,
    required: [true, "Mobile number is required"]
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  createdOn: {
    type: Date,
    default: new Date()
  },
    enrollments: [{
      courseId: {
        type: String,
        required: [true, "Course ID is required"]
      },
      courseName: {
        type: String,
        required: [true, "Course name is required"]
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: "Enrolled"
      },
      isPaid: {
        type: Boolean,
        default: true
      }
    }]
});

module.exports = mongoose.model("User", userSchema);
