const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
  courseName: {
    type: String,
    required: [true, "Course name is required"]
  },
  description: {
    type: String,
    required: [true, "Description is required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  slots: {
    type: Number,
    required: [true, "Slots is required"]
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    //the :new Date()" expression instantiates a new "date" that stores the current date and time whenever
    default: new Date()
  },
  enrollees: [{
    userId: {
      type: String,
      required: [true, "User ID is required"]
    },
    //optional field
    email: {
      type: String,
      required: [true, "email is required"]
    },
    isPaid: {
      type: Boolean,
      default: true
    },
    dateEnrolled: {
      type: Date,
      default: new Date()
    }
  }]
});


// mongoose.model("collection", schema)
module.exports = mongoose.model("Course", courseSchema);


/*course {

  id - unique for the document (auto generated)
  name,
  description,
  price,
  slots,
  isActive,
  createdOn,
  enrollees: [

    {
      id - document identifier (auto generated),
      userId,
      email,
      isPaid,
      dateEnrolled
    }
  ]

}*/
