const jwt = require("jsonwebtoken");

//used in the algorith for encrypting our data which make it difficult to decode the information without the defined secret key.
const secret = "CourseBookingAPI";

//[SECTION] JSON Web Tokens

//Token Creation

/*
  Analogy:
    Pack the gift provided wit ha lock, which can only be open using the secret code as the key.


*/
//Generate a JSON web token using the JWT's "sign method."
// /jwt.sign(payload, secretOrPrivateKey, [options, callback])
// the "user" parameter will contain the values of the user upon login
module.exports.createAccessToken = (user) => {
  console.log(user)
  //payload of the JWT
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  return jwt.sign(data, secret, {});
}


// Token Verification
/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with.
*/

module.exports.verify = (req, res, next) => {

  //The token is retrieved from the request header.
  let token = req.headers.authorization;
  //console.log(token);
  //If token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization.

  if (token !== undefined) {
    // res.send({
    //   message: "Token received!"
    // })
    //The token sent is a type of "bearer" which when received conatains the "bearer as a prefix to the string. To remove the "bearer" prefix we used the slice method retrieve only the token
    token = token.slice(7, token.length);
    console.log(token);

    //validate the "token" using the "verify" method to decrypt the token using the secret code.

    // syntax : jwt.verify(token, secretOrPrivateKey), [options/callBackFunction])

    return jwt.verify(token, secret, (err, data) => {
      // if JWT is not valid
      if (err) {
        return res.send({
          auth: "Invalid token!"
        });
      } else {
        //allows the application to proceed with the next middleware function or callback function in the route.
        next();
      }
    })

  } else {
    res.send({
      message: "Auth Failed. No token provided!"
    })

  }
}

// Token decryption
/*
	- Analogy
		Open the gift and get the content
*/

module.exports.decode = (token) => {
  if (token !== undefined) {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        //decode method is used to obtain the information from the JWT token
        // Syntax: jwt.decode(token, [options]);
        //Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
        return jwt.decode(token, {
          complete: true
        }).payload;
      }
    })
  }else{
    //If token does not exists
    return null
  }
}








//
